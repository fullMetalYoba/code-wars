package halving_sum;

public class HalvingSum {
    int halvingSum(int n) {
        int result = 0;
        int powerResult = 0;
        int exponent = 0;
        do {
            powerResult = (int) Math.pow(2, exponent);
            result = result + (n/powerResult);
            exponent++;
        }
        while ((n/powerResult) > 1);

        return result;
    }
}
