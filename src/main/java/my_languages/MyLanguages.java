package my_languages;

import java.util.*;
import java.util.stream.Collectors;

public class MyLanguages {
    public static List<String> myLanguages(final Map<String, Integer> results) {
        List<String> finalList = new ArrayList<>();

        finalList.addAll(results.entrySet().stream()
                .filter(v -> v.getValue() >= 60)
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .map(b -> b.getKey())
                .collect(Collectors.toList()));
        return finalList;
    }
}