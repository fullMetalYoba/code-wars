package snail;

public class Snail {

    public static int[] snail(int[][] array) {
        int size = getSize2dArray(array);
        int[] finalArray = new int[size];
        finalArray = extractRowFrom2dArray(array, finalArray, 0);
        finalArray = extractLastElements(array, finalArray, 1, array.length-1);
        finalArray = extractLastRow(array, finalArray);
        finalArray = fuck(array, finalArray);
        return finalArray;
    }

    private static int[] fuck(int[][] originArray, int[] targetArray){
        int rowSize = originArray[0].length;
        int columnSize = originArray.length;
        int index = targetArray.length-1 - rowSize-1 + columnSize;
        for (int i = 0; i < rowSize-1; i++){
            targetArray[index] = originArray[1][i];
            index++;
        }
        return targetArray;
    }

    private static int[] extractLastRow(int[][] originArray, int[] targetArray){
        int rowSize = originArray[0].length;
        int columnSize = originArray.length-1;
        int index = rowSize-1 + columnSize;
        int[] row = revertRow(originArray, originArray.length-1);
        for (int i = 0; i < rowSize; i++){
            targetArray[index] = row[i];
            index++;
        }
        return targetArray;
    }

    private static int[] extractLastElements(int[][] originArray, int[] targetArray, int startRowNumber,
                                             int stopRowNumber) {
        int rowSize = originArray[0].length;
        int index = rowSize;
        for (int i = startRowNumber; i < stopRowNumber; i++){
            targetArray[index] = originArray[i][rowSize-1];
            index++;
        }
        return targetArray;
    }

    private static int[] extractRowFrom2dArray(int[][] originArray, int[] targetArray, int rowNum) {
        int[] result = new int[targetArray.length];
        for (int i = 0; i < originArray[rowNum].length; i++) {
            result[i] = originArray[rowNum][i];
        }
        return result;
    }

    private static int[] get1dArrFrom2dArray(int[][] matrix) {
        int countElem = 0;
        for (int[] tmpArr : matrix) {
            countElem += tmpArr.length;
        }
        int[] ret = new int[countElem];
        int indRet = 0;
        for (int[] tmpArr : matrix) {
            for (int elemTmpArr : tmpArr) {
                ret[indRet++] = elemTmpArr;
            }
        }
        return ret;
    }

    private static int getSize2dArray(int[][] array){
        int size = 0;
        for (int[] f : array){
            size = size + f.length;
        }
        return size;
    }

    private static int[] revertRow(int[][] originArray, int rowNum){
        int[] array = new int[originArray[0].length];
        int iter = 0;
        for (int g : originArray[rowNum]) {
            array[iter] = g;
            iter++;
        }
        for (int i = 0; i < array.length / 2; i++) {
            int tmp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = tmp;
        }
        return array;
    }
}