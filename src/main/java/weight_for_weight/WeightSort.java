package weight_for_weight;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WeightSort {
    private static Pattern pattern = Pattern.compile(" ");

    public static String orderWeight(String string) {
        int[][] numbers = sumDigitsInNumberFromString(string);
        int[][] sortedArray = sortedArray(numbers, 0);
        return getBuildString(sortedArray);
    }

    private static int[][] sumDigitsInNumberFromString(String string) {
        List<String> list = extractSubStringsFromString(string);
        int array[][] = new int[list.size()][2];
        int i = array.length;

        for (String value : list) {
            Integer sum = Pattern.compile("")
                    .splitAsStream(value)
                    .mapToInt(Integer::valueOf)
                    .sum();
            int ii = array.length - i;
            int j = 0;
            array[ii][j] = sum;
            j = 1;
            array[ii][j] = Integer.parseInt(value);
            i--;
        }
        return array;
    }

    private static String getBuildString(int[][] array) {
        int[] f = new int[array.length];
        int b = 0;
        for (int[] g : array) {
            f[b] = g[1];
            b++;
        }
        String result = " ";
        for (int i = 0; i < f.length; i++) {
            if (i != f.length - 1)
                result += f[i] + " ";
            else
                result += f[i];
        }
        return result.trim();
    }

    private static int[][] sortedArray(int[][] array, int col) {
        Arrays.sort(array, new Comparator<int[]>() {
            public int compare(final int[] entry1, final int[] entry2) {
                if (entry1[col] == entry2[col]) {
                    if (entry1.hashCode() > entry2.hashCode()) {
                        return -1;
                    }
                    return 0;
                }
                if (entry1[col] > entry2[col])
                    return 1;
                else
                    return -1;
            }
        });
        return array;
    }

    private static List<String> extractSubStringsFromString(String string) {
        return Arrays.stream(pattern.split(string)).collect(Collectors.toList());
    }

    private List<Integer> extractIntegersFromString(String string) {
        return Arrays.stream(pattern.split(string)).map(m -> Integer.parseInt(m)).collect(Collectors.toList());
    }

    private int sumDigitsInNumber(int number) {
        String s = "" + number; /*преобразуем число в строку*/
        char a[] = s.toCharArray();  /*разбиваем строку на отдельные символы типа char каждый из которых засовываем в массив a[ ] -> a[0]="5", a[1]=4; a[2]=6;*/
        int ret = 0; /*объявляем переменную возврата и даем ей значение 0*/
        int len = a.length; /*вычисляем длину массива (количество рязрядов в нашем числе) и загоняем его в новую переменную len*/
        for (int i = 0; i < len; i++) { /*запускаем цикл количество оборотов которого равно длине массива данный цикл должен по очереди перебрать каждую ячейку массива а*/
            int t = (int) a[i] - 48; /*преобразуем символ числа типа char содержащегося в каждой из ячеек массива в число типа int и загоняем его значение в переменную t*/
            ret = ret + t; /*увеличиваем переменную возврата на t*/
        }
        return ret; /*завершаем работу метода возвращая финальное значение t*/
    }
}